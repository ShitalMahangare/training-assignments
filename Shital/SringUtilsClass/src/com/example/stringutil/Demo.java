package com.example.stringutil;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;

public class Demo
{
    public static void main(String[] args) throws UnsupportedEncodingException {
        String s1="Aarohi";
        String s2="Mahangareshital";
        System.out.println("Abbreviate Function");
        System.out.println(StringUtils.abbreviate("", 0, 4));
        System.out.println(StringUtils.abbreviate(null, 0, 4));

        System.out.println(StringUtils.abbreviate(s2, -1, 10));
        System.out.println(StringUtils.abbreviate(s2, 0, 10));
        System.out.println(StringUtils.abbreviate(s2, 1, 10));
        System.out.println(StringUtils.abbreviate(s2, 2, 10));
        System.out.println(StringUtils.abbreviate(s2, 3, 10));
        System.out.println(StringUtils.abbreviate(s2, 4, 10));
        System.out.println(StringUtils.abbreviate(s2, 5, 9));
        System.out.println(StringUtils.abbreviate(s2, 6, 7));
        System.out.println(StringUtils.abbreviate("", "...", 4));
        System.out.println(StringUtils.abbreviate(s2, ".", 5));
        System.out.println(StringUtils.abbreviate(s2, "..", 5));
        System.out.println(StringUtils.abbreviate(s2, ".", 7));
        System.out.println(StringUtils.abbreviate(s2, ".", 8));
        System.out.println(StringUtils.abbreviate(s2, ".", 13));
        System.out.println(StringUtils.abbreviate(s2, ".",6, 8));
        System.out.println(StringUtils.abbreviate(s2, "__", 10,8));
        System.out.println(StringUtils.abbreviate(s2, "__", 4,10));
        System.out.println(StringUtils.abbreviate(s2, "__", 9,10));
        System.out.println(StringUtils.abbreviateMiddle(s2, "_", 10));

        System.out.println("***************************************************************");
        System.out.println(StringUtils.appendIfMissing("shital",null));
        System.out.println(StringUtils.appendIfMissing("shital","tal"));
        System.out.println(StringUtils.appendIfMissing("shital","damu"));
        System.out.println(StringUtils.appendIfMissing("shital","TAL"));
        System.out.println(StringUtils.appendIfMissing("shital","Tal"));
        System.out.println("------------------------------------------------------------");
        System.out.println(StringUtils.appendIfMissingIgnoreCase("shital","Tal"));
        System.out.println(StringUtils.appendIfMissingIgnoreCase("shital","TAL"));
        System.out.println(StringUtils.appendIfMissingIgnoreCase("shital","t"));

        System.out.println("******************************************************");
        System.out.println(StringUtils.trim("     "));
        System.out.println(StringUtils.trim("   shital  "));
        System.out.println("    shital    ");
        System.out.println("******************************************************");

        System.out.println(StringUtils.capitalize("shital"));
        System.out.println(StringUtils.capitalize("Shital"));

        System.out.println("******************************************************");
        System.out.println(StringUtils.center("shital",2));
        System.out.println(StringUtils.center("shital",10));
        System.out.println(StringUtils.center("shital",10,'x'));
       System.out.println(StringUtils.center("a", 4, ' '));
       System.out.println(StringUtils.center("shital",10,"yyyy"));

        System.out.println("******************************************************");
        System.out.println(StringUtils.chomp("shital\n \r"));
        System.out.println(StringUtils.chomp("shital","tal"));
        System.out.println(StringUtils.chomp("shital","kal"));
        System.out.println("--------------------------------------------");
        System.out.println(StringUtils.chop("shital"));

        System.out.println("**********************************8");
        System.out.println(StringUtils.compare("shital","shital"));
        System.out.println(StringUtils.compare("shital","shi"));
        System.out.println(StringUtils.compare("a","b"));

        System.out.println("-------------------------------");
       System.out.println(StringUtils.compare(null , "a", true));
        System.out.println(StringUtils.compare(null , "a", false));
        System.out.println(StringUtils.compare("a", null, true));
                ;
        System.out.println(StringUtils.compare("a", null, false));

        System.out.println( StringUtils.compareIgnoreCase("a", "b"));
        System.out.println(StringUtils.compareIgnoreCase("b", "a") )      ;
        System.out.println(StringUtils.compareIgnoreCase("a", "B")  )     ;
        System.out.println(StringUtils.compareIgnoreCase("A", "b")   )   ;
        System.out.println(StringUtils.compareIgnoreCase("ab", "abc") )   ;

        System.out.println("****************************************");
        System.out.println("The contains function");
        System.out.println(StringUtils.contains("shital",""));
        System.out.println(StringUtils.contains("shital",null));
        System.out.println(StringUtils.contains("shital","sh"));
        System.out.println(StringUtils.contains("shital","ai"));
        System.out.println(StringUtils.contains("shital",'a'));
        System.out.println("_______-----------------------------");
        System.out.println(StringUtils.containsAny("shital",'a','b'));
        System.out.println(StringUtils.containsAny("shital","sh","al"));
        System.out.println(StringUtils.containsAny("shital", "$.#yF"));
       // StringUtils.containsAny("zzabyycdxx", ['z', 'a'])
        System.out.println("______________________________________________");
        System.out.println(StringUtils.containsIgnoreCase("shital","a"));
        System.out.println(StringUtils.containsIgnoreCase("shital","T"));

        System.out.println("---------------------------------------------------------");
        System.out.println(StringUtils.containsNone("shital","shi"));
        System.out.println(StringUtils.containsNone("ab1", "xyz"));
        System.out.println(StringUtils.containsNone("ab1", "abc"));

        System.out.println("*************************************");
        System.out.println("Contains White space");
        System.out.println(StringUtils.containsWhitespace("shital"));
        System.out.println(StringUtils.containsWhitespace("shital "));
        System.out.println(StringUtils.containsWhitespace(null));
        System.out.println(StringUtils.containsWhitespace(" "));

        System.out.println("****************************************");
        System.out.println(StringUtils.countMatches("shital",'s'));
        System.out.println(StringUtils.countMatches("shital","lak"));
        System.out.println(StringUtils.countMatches("abcabdfabj","ab"));

        System.out.println("******************************************");
        System.out.println("Default if Blank");
        System.out.println(StringUtils.defaultIfBlank(" ",null));
        System.out.println(StringUtils.defaultIfBlank(" ","shital"));
        System.out.println(StringUtils.defaultIfBlank("shital","kajal"));
        System.out.println("______________________________________________");
        System.out.println(StringUtils.defaultIfEmpty(" ",null));
        System.out.println(StringUtils.defaultIfEmpty(" ","shital"));
        System.out.println(StringUtils.defaultIfEmpty("shital","kajal"));

        System.out.println("************************************************");
        System.out.println("Ends With Any");
        System.out.println(StringUtils.endsWithAny("shital" ,new String[]{"sh","al"}));

        System.out.println("**********************************************");
        System.out.println("Equals method");
        String n1="welcome";
        String  n2="welcome";
        if(StringUtils.equals(n1,n2))
        {
            System.out.println("true");
        }
        else
        {
            System.out.println("false");
        }

        System.out.println("*********************************************");

    }
}
